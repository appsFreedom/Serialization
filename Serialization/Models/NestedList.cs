﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Serialization.Models
{
    [DataContract]
    public class NestedList<T>
    {
        [DataMember(Name = "mainItem")]
        public T Item { get; set; }

        [DataMember(Name = "subItems")]
        public List<T> SubItems { get; set; }
    }
}
