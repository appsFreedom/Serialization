﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Serialization.Models
{
    [DataContract]
    public class NestedDictionary<V, T>
    {
        [DataMember(Name = "mainItem")]
        public T Item { get; set; }

        [DataMember(Name = "subItems")]
        public Dictionary<V, T> SubItems { get; set; }
    }
}
