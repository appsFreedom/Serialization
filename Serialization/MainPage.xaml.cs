﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Serialization.Models;
using Newtonsoft.Json;
using System.Diagnostics;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Serialization
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // Set breakpoints at the end of every Test*** method
            // and see values of "json" and "deserializedObject" variables.

            TestItem();
            TestList();
            TestNestedList();
            TestDictionary();
            TestNestedDictionary();
        }

        public void TestItem()
        {
            var person = new Person("Jack", 35);

            var json = JsonConvert.SerializeObject(person);
            var deserializedObject = JsonConvert.DeserializeObject<Person>(json);
        }

        public void TestList()
        {
            var list = new List<Person>()
            {
                new Person("Jack", 35),
                new Person("Kevin", 25),
                new Person("Sarah", 43),
                new Person("Michael", 19),
            };

            var json = JsonConvert.SerializeObject(list);
            var deserializedObject = JsonConvert.DeserializeObject<List<Person>>(json);
        }

        public void TestNestedList()
        {
            var nestedList = new NestedList<Person>()
            {
                Item = new Person("Jack", 35),
                SubItems = new List<Person>()
                {
                    new Person("Kevin", 25),
                    new Person("Sarah", 43),
                    new Person("Michael", 19),
                },
            };

            var json = JsonConvert.SerializeObject(nestedList);
            var deserializedObject = JsonConvert.DeserializeObject<NestedList<Person>>(json);
        }

        public void TestDictionary()
        {
            var dict = new Dictionary<int, Person>()
            {
                { 1, new Person("Jack", 35) },
                { 2, new Person("Kevin", 25) },
                { 3, new Person("Sarah", 43) },
                { 4, new Person("Michael", 19) }
            };

            var json = JsonConvert.SerializeObject(dict);
            var deserializedObject = JsonConvert.DeserializeObject<Dictionary<int, Person>>(json);
        }

        public void TestNestedDictionary()
        {
            var nestedDict = new NestedDictionary<int, Person>()
            {
                Item = new Person("Jack", 35),
                SubItems = new Dictionary<int, Person>()
                {
                    { 1, new Person("Kevin", 25) },
                    { 2, new Person("Sarah", 43) },
                    { 3, new Person("Michael", 19) }
                },
            };

            var json = JsonConvert.SerializeObject(nestedDict);
            var deserializedObject = JsonConvert.DeserializeObject<NestedDictionary<int, Person>>(json);
        }
    }
}